namespace HelloWorld
{
    using System;
    using System.Reactive.Linq;
    using System.Reactive.Subjects;
    using HelloWorld;
    using HelloWorld.IObservableIObserverImpl;
    using HelloWorld.IObservableFactorySample;
    
    
    class Program
    {
        static void Main(string[] args)
        {
            /*
                その２
            */
            // Console.WriteLine("\n===== Main2_1 =====");
            // Program.Main2_1(args);
            // Console.WriteLine("\n===== Main2_2 =====");
            // Program.Main2_2(args);

            /*
                その３
            */
            // Console.WriteLine("\n===== Main3_1 =====");
            // Program.Main3_1(args);
            
            /*
                その４
            */
            // Console.WriteLine("\n===== Main4_1 =====");
            // Program.Main4_1(args);

            /*
                その５ Hot Cold
            */
            // Console.WriteLine("\n===== Main5_1 =====");
            // Program.Main5_1(args);

            /*
                その６ 
            */
            Console.WriteLine("\n===== Main6_1 =====");
            Program.Main6_1(args);

            /*
                その７
            */
            // Console.WriteLine("\n===== Main7_1 =====");
            // Program.Main7_1(args);
        }

        /// <summary>
        /// IObserver, IObservableで実装した版
        /// </summary>
        /// <param name="args"></param>
        static void Main2_1(string[] args)
        {
        // 監視される側を生成
        var source = new NumberObservable();

        // 監視役を2つ登録する
        var subscriber1 = source.Subscribe(new PrintObserver());
        var subscriber2 = source.Subscribe(new PrintObserver());

        // 監視される側の処理を実行する
        Console.WriteLine("## Execute(1)");
        source.Execute(1);
        // 監視役を1つ解雇する

        Console.WriteLine("## Dispose");
        subscriber2.Dispose();

        // 再度、処理の実行
        Console.WriteLine("## Execute(2)");
        source.Execute(2);

        // エラーを起こしてみる
        Console.WriteLine("## Execute(0)");
        source.Execute(0);

        // 完了通知
        // 監視役を1つ追加し、完了通知を行う
        var subscriber3 = source.Subscribe(new PrintObserver());
        Console.WriteLine("## Completed");
        source.Completed();
        }

        /// <summary>
        /// Rxで書き直した版
        /// </summary>
        static void Main2_2(string[] args)
        {
            // 監視される人を作成
            var source = new NumberObservable();

            // 2つの監視役を登録する
            // 引数を3つ取るタイプのSubscribe拡張メソッドを使用する
            var subscriber1 = source.Subscribe(
                // OnNext
                value => Console.WriteLine($"OnNext({value}) called", value),
                // OnError
                ex => Console.WriteLine("OnError({0})", ex.Message),
                // OnCompleted
                () => Console.WriteLine("OnCompleted() called"));

            var subscriber2 = source.Subscribe(
                value => Console.WriteLine($"OnNext({value}) called", value),
                // OnError
                ex => Console.WriteLine("OnError({0})", ex.Message),
                // OnCompleted
                () => Console.WriteLine("OnCompleted() called")
            );

            // 監視される人の処理を実行する
            Console.WriteLine("## Execute(1)");
            source.Execute(1);
            
            // 監視する人を一人解雇
            Console.WriteLine("## Dispose");
            subscriber2.Dispose();

            // 再度処理を実行
            Console.WriteLine("## Execute(2)");
            source.Execute(2);

            // エラーを起こしてみる
            Console.WriteLine("## Execute(0)");
            source.Execute(0);

            // 監視役を1つ追加して、完了通知を行う
            var subscriber3 = source.Subscribe(
                value => Console.WriteLine($"OnNext({value}) called", value),
                // OnError
                ex => Console.WriteLine("OnError({0})", ex.Message),
                // OnCompleted
                () => Console.WriteLine("OnCompleted() called")
            );

            Console.WriteLine("## Completed");
            source.Completed();
            
        }

        /// <summary>
        /// IObservableのファクトリメソッドの動作確認
        /// </summary>
        /// <param name="args"></param>
        static void Main3_1(string[] args)
        {
            var iObservableFactory = new IObservableFactory();
            iObservableFactory.CalledFromMain();
        }

        /// <summary>
        /// IObservableのファクトリメソッドの動作確認(Timer系)
        /// </summary>
        /// <param name="args"></param>
        static void Main4_1(string[] args)
        {
            var iObservaleFactoryTime = new IObservableFactoryTime();
            // iObservaleFactoryTime.TimerSample();
            // iObservaleFactoryTime.GenerateSample();
            // iObservaleFactoryTime.IntervalSample();
        }

        static void Main5_1(string[] args)
        {
            var hotColdSample = new HotColdSample.HotColdSample();
            hotColdSample.ColdSample();
            hotColdSample.HotSample();
        }       
        static void Main6_1(string[] args)
        {
            var LinqStyleMethodSample = new LinqStyleExMethodSample.LinqStyleExMethodSample();
            LinqStyleMethodSample.LinqStyleSample();
            LinqStyleMethodSample.SkipTakeSample();
            LinqStyleMethodSample.SkipUntilTakeUntilSample();
        
        }
    }
}