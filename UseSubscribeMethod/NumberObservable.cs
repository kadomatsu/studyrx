namespace HelloWorld.UseSubscribeMethvoid 
{
    using System;
    using System.Reactive.Subjects;
    class NumberObservable : IObservable<int>
    {
        // IObservable<T>とIObserver<T>の両方ともを兼ねるクラス
        private Subject<int> source = new Subject<int>();

        // 自分を監視している人たちに通知を行う
        // 0 ならエラーを通知する
        public void Execute(int value)
        {
            if (value == 0)
            {
                this.source.OnError(new Exception("value is Zero"));
                // まっさらなSubjectを再生成する
                this.source = new Subject<int>();
            }

            this.source.OnNext(value);
        }

        // 完了通知
        public void Completed()
        {
            this.source.OnCompleted();
        }

        // 監視している人の追加
        // 戻り値のIDisposableをDisposeすると、監視から外れる。
        public IDisposable Subscribe(IObserver<int> observer)
        {
            return this.source.Subscribe(observer);
        }
    }
}