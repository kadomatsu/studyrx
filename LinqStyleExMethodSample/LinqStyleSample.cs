namespace HelloWorld.LinqStyleExMethodSample
{
    using System;
    using System.Reactive.Linq;
    using System.Reactive.Subjects;

    class LinqStyleExMethodSample
    {
        public void LinqStyleSample()
        {
            /*
                where, select Sample
            */
            // 値を発行するためのSubjectを作成する
            var subject = new Subject<int>();
            
            // Iobservableに変換する（ダウンキャストで戻せないようになる）
            var source = subject.AsObservable(); // HOT

            // subscする
            source.Subscribe(
                value => Console.WriteLine($"1##OnNext({value})"),
                ex => Console.WriteLine(ex.Message),
                () => Console.WriteLine("1##OnCompleted()")
            );

            source.Where(i => i % 2 == 1) // 奇数のみ通すようにフィルタリング
                .Select(i => i + "は奇数です") // 文字列に加工
                // 表示する
                .Subscribe(
                    value => Console.WriteLine($"2##OnNext({value})"),
                    ex => Console.WriteLine(ex.Message),
                    () => Console.WriteLine("2##OnCompleted()")
                );
            // サポートされない記法になってしまったらしいが、よくわからんので一旦これで書く
            Observable.Range(1, 10).ForEach(i => subject.OnNext(i));

            // 完了通知を行う
            subject.OnCompleted();
        }

            /*
                Skip ,Take
            */
        public void SkipTakeSample()
        {
            var subject2 = new Subject<int>();

            // はじめの3つをスキップしてその後の3つを通知する
            subject2.Skip(3)
                .Take(3)
                .Subscribe(
                    i => Console.WriteLine($"OnNext({i})"),
                    ex => Console.WriteLine($"OnError({ex.Message})"),
                    () => Console.WriteLine("OnCompleted()")
                );

            // 1-10の値を流し込む
            // Observable.Range(1, 10).ForEach(i => subject2.OnNext(i));

            /*
                Repeatとの組み合わせ
            */
            var subject3 = new Subject<int>();

            subject3.Skip(3).Take(3).Repeat(3).Subscribe(
                i => Console.WriteLine($"OnNext({i})"),
                ex => Console.WriteLine($"OnError({ex.Message})"),
                () => Console.WriteLine("OnCompleted()")
            );

            Observable.Range(1,20).ForEach(i => subject3.OnNext(i));
        }

        /*
            SkipWhile TakeWhile 
        */
        public void SkipWhileTakeWhile()
        {
            var s4 = new Subject<int>();

            s4.SkipWhile(i => i < 5)
                .TakeWhile(i => i < 10)
                .Subscribe(
                    i => Console.WriteLine("OnNext({0})", i),
                    ex => Console.WriteLine("OnError({0})", ex.Message),
                    () => Console.WriteLine("OnCompleted()")
                );
            
            Console.WriteLine("1-20 OnNext start");
            Observable.Range(1, 20).ForEach(i => s4.OnNext(i));
            Console.WriteLine("1-20 OnNext end"); 
        }

        /*
            SkipUntil TakeUntil
        */
        public void SkipUntilTakeUntilSample()
        {
            var s5 = new Subject<int>();
            var startTrigger = new Subject<System.Reactive.Unit>();
            var endTrigger = new Subject<System.Reactive.Unit>();
        
            // startTriggerのOnNextが呼ばれるまで値をスキップする
            // endTriggerのOnNextが呼ばれるまで、後続に値を渡す
            s5.SkipUntil(startTrigger).TakeUntil(endTrigger).Subscribe(
                i => Console.WriteLine("OnNext({0})", i),
                ex => Console.WriteLine("OnError({0})", ex.Message),
                () => Console.WriteLine("OnCompleted()")
            );

            // 1-5を流し込む
            Console.WriteLine("1-5 OnNext start.");
            Observable.Range(1, 5).ForEach(i => s5.OnNext(i));
            Console.WriteLine("1-5 OnNext End.");
            Console.WriteLine();

            // startTriggerを起動後に1-5を流しこむ
            Console.WriteLine("1-5 OnNext start.");
            startTrigger.OnNext(System.Reactive.Unit.Default);
            Observable.Range(1, 5).ForEach(i => s5.OnNext(i));
            Console.WriteLine("1-5 OnNext End.");
            Console.WriteLine();
            
            // endTriggerを起動後に1-5に流し込む
            Console.WriteLine("1-5 OnNext start.");
            endTrigger.OnNext(System.Reactive.Unit.Default);
            Observable.Range(1,5).ForEach(i => s5.OnNext(i));
            Console.WriteLine("1-5 OnNext End.");
        }
    }
}

