namespace HelloWorld.HotColdSample
{

    using System;
    using System.Reactive.Linq;
    using System.Reactive.Subjects;

    class HotColdSample
    {
        public void ColdSample()
        {
            /*
                Cold
            */
            var subject1 = new Subject<string>();

            // subjectから生成されたObservableは【HOT】
            var sourceObservable1 = subject1.AsObservable();

            // ストリームに流れてきた文字列を連結して新しい文字列にストリーム
            // Scan()は【Cold】
            var stringObservable1 = sourceObservable1.Scan((p, c) => p + c);

            // ストリームに値を流す
            subject1.OnNext("A");
            subject1.OnNext("B");

            // ストリームに値を流したあとにsubscribe
            stringObservable1.Subscribe(Console.WriteLine);

            // Subscribeあとにストリームに 値を流す
            subject1.OnNext("C");

            // 完了通知
            subject1.OnCompleted();

            // 表示
            // C
        }

        public void HotSample()
        {
            /*
                Hot
            */
            var subject2 = new Subject<string>();

            // subjectから生成されたIObserverは【Hot】である
            var sourceObservable2 = subject2.AsObservable();

            // ストリームに流れてきた文字列を連結するストリーム
            // Scan()は 【Cold】
            var stringObservable2 = sourceObservable2
                .Scan((p, c) => p + c)  // ここは【Cold】
                .Publish();             // ここで【Hot】に変換

            stringObservable2.Connect(); // ストリームの稼働開始
            
            //ストリームに値を流す
            subject2.OnNext("A");
            subject2.OnNext("B");

            // ストリームに値を流したあとにSubscribe
            stringObservable2.Subscribe(Console.WriteLine);

            subject2.OnNext("C");

            subject2.OnCompleted(); 

            // 表示
            // ABC
        }
    }
}