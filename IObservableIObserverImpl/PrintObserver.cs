namespace HelloWorld.IObservableIObserverImpl
{
    using System;
    // 監視する人
    class PrintObserver : IObserver<int>
    {
        public void OnNext(int value)
        {
            Console.WriteLine($"OnNext({value}) called.");
        }

        public void OnCompleted()
        {
            Console.WriteLine($"OnCompleted called");
        }

        public void OnError(Exception Error)
        {
            Console.WriteLine($"OnError called");
        }
    }
}