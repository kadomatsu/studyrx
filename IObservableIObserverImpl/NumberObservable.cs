
namespace HelloWorld.IObservableIObserverImpl
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 監視される側のクラス
    /// </summary>
    class NumberObservable : IObservable<int>
    {
        // 監視している側を管理するリスト
        private List<IObserver<int>> observers = new List<IObserver<int>>();

        // 監視している人たちに通知を行う
        // エラーの場合は、0を返す
        public void Execute(int value)
        {
            if (value == 0)
            {
                foreach (var obs in observers)
                {
                    obs.OnError(new Exception("value is 0"));
                }

                // エラーが発生したので、処理は終了
                this.observers.Clear();
                return;
            }

            foreach (var obs in observers)
            {
                obs.OnNext(value);
            }
        }
        // 完了通知
        public void Completed()
        {
            foreach (var obs in observers)
            {
                obs.OnCompleted();
            }
            // 完了したので、監視している人をクリアする
            this.observers.Clear();
        }


        // 監視している人を追加する
        // 戻り地のIDisposableをDisposeすると監視から外れる
        public IDisposable Subscribe(IObserver<int> observer)
        {
            this.observers.Add(observer);
            return new RemoveListDisposable(observers, observer);
        }

        // Disposeが呼ばれたらobserverを監視者から外す
        private class RemoveListDisposable : IDisposable
        {
            private List<IObserver<int>> observers = new List<IObserver<int>>();
            private IObserver<int> observer;
            public RemoveListDisposable(List<IObserver<int>> observers, IObserver<int> observer)
            {
                this.observers = observers;
                this.observer = observer;
            }

            // リソースの開放
            // 単純にリストから削除するだけ
            public void Dispose()
            {
                if (this.observers == null)
                {
                    return;
                }

                // 監視する側リストに存在する場合、リストから削除する
                // IndexOf関数は、リスト内に検索対象が見つからない場合-1を返す
                if (observers.IndexOf(observer) != -1)
                {
                    this.observers.Remove(observer);
                }

                this.observers = null;
                this.observer = null;
            }
        }
    }
}