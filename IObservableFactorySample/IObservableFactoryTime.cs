namespace HelloWorld.IObservableFactorySample
{
    using System;
    using System.Reactive.Linq;
    using System.Reactive.Subjects;
    class IObservableFactoryTime
    {
        public void TimerSample()
        {
            Console.WriteLine("\n__Timer__");
            var source1 = Observable.Timer(
                TimeSpan.FromSeconds(3),    // 開始までの待ち時間
                TimeSpan.FromSeconds(1)    // 発行間隔
            );

            //購読
            var subscription = source1.Subscribe(
                i => Console.WriteLine($"OnNext({i})"),
                ex => Console.WriteLine($"OnError({ex.Message})"),
                () => Console.WriteLine("Completed()")
            );

            Console.WriteLine("plz enter key...");
            Console.ReadLine();
            Console.WriteLine("dispose method call.");
            subscription.Dispose();
            // Disposeすると、値が発行されなくなる
            Console.WriteLine("plz enter key...");
            Console.ReadLine();

        }

        public void IntervalSample()
        {
            Console.WriteLine("\n__Interval__");

            // IObservableの生成
            var source = Observable.Interval(
                TimeSpan.FromMilliseconds(500) // 実行間隔
            );

            var subscription2 = source.Subscribe(
                i => Console.WriteLine($"OnNext({i})"),
                ex => Console.WriteLine($"OnError({ex.Message})"),
                () => Console.WriteLine("Completed")
            );

            Console.WriteLine("plz enter key...");
            Console.ReadLine();
            // キー入力があったら、Disposeを行う
            Console.WriteLine("dispose method call.");
            subscription2.Dispose();
            Console.WriteLine("plz enter key...");
            Console.ReadLine();
        }

        public void GenerateSample()
        {
            Console.WriteLine("\n__Generate__");
            var source3 = Observable.Generate(
                0,           // 初期値
                i => i < 10, // 継続条件
                i => ++i,    // iをインクリメント
                i => i * i,  // 発行する値
                i => TimeSpan.FromMilliseconds(500)
            );

            var subscription3 = source3.Subscribe(
                i => Console.WriteLine("OnNext({0})", i),
                ex => Console.WriteLine("OnError({0})", ex.Message),
                () => Console.WriteLine("Completed()")
            );

            Console.WriteLine("please enter key...");
            Console.ReadLine();
            // Observableが発行する値の購読を停止
            Console.WriteLine("dispose method call.");
            subscription3.Dispose();
            // Disposeをすると値が発行されても受け取らなくなる。
            Console.WriteLine("please enter key...");
            Console.ReadLine();
        }
    }
}