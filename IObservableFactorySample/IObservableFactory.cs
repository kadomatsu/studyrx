namespace HelloWorld.IObservableFactorySample
{
    using System;
    using System.Reactive.Subjects;
    using System.Reactive.Linq;

    public class IObservableFactory
    {
        public void CalledFromMain(){
            /*
                Return<TResult>(TResult)
                1要素のみ持つ observable sequenceを返す
            */
            Console.WriteLine("\n__Return__");
            var source1 = Observable.Return(10);
            var subscription1 = source1.Subscribe(
                i => Console.WriteLine($"OnNext({i})"),
                ex => Console.WriteLine("ONError{0}", ex.Message),
                () => Console.WriteLine("Completed()")
            );
            // 購読の停止
            subscription1.Dispose();

            /*
                Repeat<TResult>(TResult, Int32)
                同じ値を指定回数返す
            */
            Console.WriteLine("\n__Repeat__");
            var source2 = Observable.Repeat(2, 5);
            var subscription2 = source2.Subscribe(
                i => Console.WriteLine($"OnNext({i})"),
                ex => Console.WriteLine("ONError{0}", ex.Message),
                () => Console.WriteLine("Completed()")
            );
            // 購読の停止
            subscription2.Dispose();

             /*
                Range()
                指定した値から1ずつカウントアップした値を返す
             */
            Console.WriteLine("\n__Range__");
            var source3 = Observable.Range(1, 10);
            var subscription3 = source3.Subscribe(
                i => Console.WriteLine($"OnNext({i})"),
                ex => Console.WriteLine("ONError{0}", ex.Message),
                () => Console.WriteLine("Completed()")
            );
            // 購読の停止
            subscription3.Dispose();

            /*
                IObservableの拡張メソッドのRepeat
                IObservableが発行する値をさらに繰り返したIObservableを作成できる
            */
            Console.WriteLine("\n__Expanded Repeat__");
            var source4 = Observable.Range(1, 3);
            source4 = source4.Repeat(3);
            var subscription4 = source4.Subscribe(
                i => Console.WriteLine($"OnNext({i})"),
                ex => Console.WriteLine("ONError{0}", ex.Message),
                () => Console.WriteLine("Completed()")
            );
            // 購読の停止
            subscription4.Dispose();

            /*
                Generate()
                for文っぽい使用感のあるメソッド
            */
            Console.WriteLine("\n__Generate__");
            var source5 = Observable.Generate(
                0,          // 初期値
                i => i < 6, // 継続条件
                i => ++i,   // 更新処理（インクリメント）
                i => i * i  // 発行する値の生成処理
            );

            source5 = source5.Repeat(3);
            var subscription5 = source5.Subscribe(
                i => Console.WriteLine($"OnNext({i})"),
                ex => Console.WriteLine("ONError{0}", ex.Message),
                () => Console.WriteLine("Completed()")
            );
            // 購読の停止
            subscription5.Dispose();

            /*
                Defer()
                引数に、IObservableを直接返すラムダ式を渡す
                Subscribeメソッドが呼ばれる度、Deferメソッドが実行され、IObservableが作成される
                ReturnはObservableが作成された瞬間に実行されるが、DeferはSubscribeされるまで処理の実行を待つことができる
            */
            Console.WriteLine("\n__Defer__");
            var source6 = Observable.Defer<int>(
                () => 
                    {
                        Console.WriteLine("# Defer method called.");
                        // ReplaySubject<T>はSubject<T>の亜種
                        // 今までの操作をすべてリプレイする
                        // namespace  System.Reactive.Subjects
                        var s = new ReplaySubject<int>();
                        s.OnNext(1);
                        s.OnNext(2);
                        s.OnNext(3);
                        s.OnCompleted();
                        // AsObservableメソッドでReplaySubjectをIObservableに変換できる
                        return s.AsObservable(); 
                    } 
            );
            // 購読
            // sourceをReplaySubjectで作成しているため、Defer内で行った操作を再生する
            var subscription6_1 = source6.Subscribe(
                i => Console.WriteLine("OnNext({0})", i),
                ex => Console.WriteLine("OnError({0})", ex.Message),
                () => Console.WriteLine("Completed()")
            );
            var subscription6_2 = source6.Subscribe(
                i => Console.WriteLine("OnNext({0})", i),
                ex => Console.WriteLine("OnError({0})", ex.Message),
                () => Console.WriteLine("Completed()")
            );

            // 購読停止
            subscription6_1.Dispose();
            subscription6_2.Dispose();


            /*
                Createメソッド
                引数は、IObserverを受け取ってActionを返すラムダ
            */
            Console.WriteLine("\n__Create__");
            var source7 = Observable.Create<int>(observer => 
                {
                    Console.WriteLine("# Create method called.");
                    // 引数のIObserver<int>に対して、On***メソッドを呼ぶ
                    observer.OnNext(1);
                    observer.OnNext(2);
                    observer.OnNext(3);
                    observer.OnCompleted();
                    // Disposeが呼ばれたときの処理を返す
                    // リソースを確保していた場合は、ここを開放すれば良い
                    return () => Console.WriteLine("Disposable action");
                }
            );
            // 購読
            var subscription7_1 = source7.Subscribe(
                i => Console.WriteLine($"OnNext{i}"),
                ex => Console.WriteLine($"OnError{ex.Message}"),
                () => Console.WriteLine("Completed()")
            );
            var subscription7_2 = source7.Subscribe(
                i => Console.WriteLine($"OnNext{i}"),
                ex => Console.WriteLine($"OnError{ex.Message}"),
                () => Console.WriteLine("Completed()")
            );

            // 購読停止 意味無し
            Console.WriteLine("# Dispose method call.");
            subscription7_1.Dispose();
            subscription7_2.Dispose();

            // "Disposable action"
            // "# Dispose method call."の順に表示される
            // Completedの直後、Dispose が自動的に行われているため
            
            /*
                Throwメソッド
                引数に例外を渡す
            */
            Console.WriteLine("\n__Create__");
            var source8 = Observable.Throw<int>(new Exception("Error meassage"));
            // 購読
            var subscription8 = source8.Subscribe(
                i => Console.WriteLine($"OnNext({i})"),
                ex => Console.WriteLine($"OnError({ex.Message})"),
                () => Console.WriteLine("Completed()")
            );

            subscription8.Dispose();
        }
    }
}
